# DynamicNft

## Contract

Usage:
- Please refer to `contract/README.md`

## Backend (dynamic metadata)

Used to generate `tokenURI` dynamically.

## App (UI)

- Test: https://d2bnfxfxzijfou.cloudfront.net/
- OpenSea: https://testnets.opensea.io/collection/dynamicemotes-v2
- Rarible: https://testnet.rarible.com/collection/0x225338e2f1309902af97bf925422e1334d05e51e/items
