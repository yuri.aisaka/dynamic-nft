// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import "@openzeppelin/contracts/utils/Base64.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

import "hardhat/console.sol";

// import "@openzeppelin/contracts/utils/Counters.sol";

contract OnchainData {
    constructor(string memory baseUrl_) {
        baseUrl = baseUrl_;
    }

    string baseUrl;

    function getURI(uint256 emoteId, uint64 likes)
        public
        view
        returns (string memory)
    {
        // string.concat() since solidity 8.12
        return
            string.concat(
                string.concat(
                    string.concat(baseUrl, Strings.toString(emoteId)),
                    "/likes/"
                ),
                Strings.toString(likes)
            );
    }
}

contract EmoteFactory is ERC721 {
    using SafeMath for uint64;

    event NewEmote(address sender, uint256 emoteId, string name, uint64 likes);
    event NewLike(uint256 emoteId, uint64 likes);

    struct Emote {
        string name;
        uint64 likes;
    }

    Emote[] public emotes;

    // mapping(uint256 => address) public emoteOwner;
    mapping(address => uint64) public ownerEmoteCount;
    OnchainData onchainData;

    constructor(
        string memory _baseUrl,
        string memory _name,
        string memory _symbol
    ) ERC721(_name, _symbol) {
        onchainData = new OnchainData(_baseUrl);
    }

    function like(uint256 emoteId) public {
        uint64 likes = emotes[emoteId].likes + 1;
        emotes[emoteId].likes = likes;

        emit NewLike(emoteId, likes);
    }

    // IERC721Metadata
    // function name() public view override returns (string memory) {
    //     return this._name;
    // }
    //
    // function symbol() public view override returns (string memory) {
    //     return this._symbol;
    // }

    // application/json from tokenURI could be embedded as data:application/json as well
    function tokenURI(uint256 emoteId)
        public
        view
        override
        returns (string memory)
    {
        string memory url = onchainData.getURI(emoteId, emotes[emoteId].likes);
        // string memory json = Base64.encode(
        //     abi.encodePacked(
        //         '{"name": "NFT#',
        //         Strings.toString(emoteId),
        //         '", "attributes": [], "description": "test", "image": "',
        //         url,
        //         '"}'
        //     )
        // );
        // return string(abi.encodePacked("data:application/json;base64,", json));
        return url;
    }

    function mintNewEmote() external {
        uint256 emoteId = emotes.length;
        string memory name = string.concat("NFT#", Strings.toString(emoteId));
        uint64 likes = 0;

        emotes.push(Emote(name, likes));
        _safeMint(msg.sender, emoteId);

        // emoteOwner[emoteId] = msg.sender;
        ownerEmoteCount[msg.sender] = ownerEmoteCount[msg.sender] + 1;
        console.log(
            "Minted NFT with tokenId %s for ",
            emoteId,
            ownerOf(emoteId)
        );
        emit NewEmote(msg.sender, emoteId, name, likes);
    }

    function getAllEmotes() public view returns (Emote[] memory) {
        return emotes;
    }

    function listEmoteIdsOf(address _owner)
        external
        view
        returns (uint256[] memory)
    {
        uint256[] memory ids = new uint256[](ownerEmoteCount[_owner]);
        uint256 count = 0;
        for (uint256 i = 0; i < emotes.length; ++i) {
            if (ownerOf(i) == _owner) {
                ids[count] = i;
                ++count;
            }
        }
        return ids;
    }
}
