import { ethers } from "hardhat";

async function main() {
  const [owner, user0, user1, user2] = await ethers.getSigners();
  console.log("Contract owner: %s", owner);

  const baseUrl =
    "https://w6wbwgfv5iqxm5b5opjc5wjkza0fpqco.lambda-url.ap-northeast-1.on.aws/";
  const emoteFactory = await ethers.getContractFactory("EmoteFactory");
  const emoteContract = await emoteFactory.deploy(
    baseUrl,
    "TestEmoteFactory",
    "ememe"
  );
  await emoteContract.deployed();
  console.log("Contract deployed to: ", emoteContract.address);

  const tx0 = await emoteContract.connect(user0).mintNewEmote();
  const log0 = await tx0.wait();
  console.log("Mint log: %s", log0);
  const uri0 = await emoteContract.tokenURI(0); // ERC720
  console.log("URI: ", uri0);

  const tx1 = await emoteContract.connect(user1).mintNewEmote();
  const log1 = await tx1.wait();
  console.log("Mint log: %s", log1);
  const uri1 = await emoteContract.tokenURI(1);
  console.log("URI: ", uri1);

  const tx02 = await emoteContract.connect(user2).like(0);
  const log02 = await tx02.wait();
  console.log("Like log: %s", log02);

  const tx12 = await emoteContract.connect(user2).like(1);
  const log12 = await tx12.wait();
  console.log("Like log: %s", log12);

  const tx01 = await emoteContract.connect(user1).like(0);
  const log01 = await tx01.wait();
  console.log("Like log: %s", log01);

  const uri0a = await emoteContract.tokenURI(0);
  const uri1a = await emoteContract.tokenURI(1);
  console.log("URI0: ", uri0a);
  console.log("URI1: ", uri1a);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
