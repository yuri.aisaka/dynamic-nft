// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers } from "hardhat";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const [owner] = await ethers.getSigners();
  console.log("Contract owner: %s", owner);

  // const baseUrl =
  //   "https://exp-ememe-dancefloor-mvp25-emotes.s3.ap-northeast-1.amazonaws.com/";
  const baseUrl =
    "https://w6wbwgfv5iqxm5b5opjc5wjkza0fpqco.lambda-url.ap-northeast-1.on.aws/";

  const emoteFactory = await ethers.getContractFactory("EmoteFactory");
  const emoteContract = await emoteFactory.deploy(
    baseUrl,
    "DynamicEmotes",
    "DYNEMO"
  );
  await emoteContract.deployed();
  console.log("Contract deployed to: ", emoteContract.address);

  const tx0 = await emoteContract.mintNewEmote();
  const log0 = await tx0.wait();
  console.log("Mint log: %s", log0);
  const uri0 = await emoteContract.tokenURI(0); // ERC720
  console.log("URI: ", uri0);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
