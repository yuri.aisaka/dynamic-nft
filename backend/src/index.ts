import { MOODS, SILLY_WORDS, COLORS } from "./consts";
import { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from "aws-lambda";
import { Buffer } from "buffer";

interface Args {
  emoteId: number;
  likes: number;
}

interface Attrs {
  emoteId: number;
  likes: number;
  level: number;
  mood: string;
  desc: string;
}

const parse = (path: string): Args => {
  // path = /<emoteId>/likes/<likes>
  const elems = path.split("/");
  const emoteId = parseInt(elems[1]);
  const likes = parseInt(elems[3]);
  return {
    emoteId,
    likes,
  };
};

const choose = <T>(arr: readonly T[]): T => {
  return arr[Math.floor(Math.random() * arr.length)];
};

const genDesc = (mood: string): string => {
  return `${mood} ${choose(SILLY_WORDS)}`;
};

const toAttrs = (args: Args): Attrs => {
  const mood = choose(MOODS);
  const desc = genDesc(mood);
  return {
    emoteId: args.emoteId,
    likes: args.likes,
    level: Math.floor(args.likes / 10), // FIXME hard coded
    mood,
    desc,
  };
};

const svg = (emoteId: number, level: number, likes: number): string => {
  const bg = COLORS[Math.floor((emoteId * 23) % COLORS.length)];
  const text = `<text x='70%' y='70%' class='base' dominant-baseline='middle' text-anchor='middle'>${emoteId}</text>`;
  const text2 = `<text x='80%' y='10%' class='annot' dominant-baseline='middle' text-anchor='middle'>${likes} likes!</text>`;
  return `<svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMinYMin meet' viewBox='0 0 256 256'>
       <style>.base { fill: white; font-family: serif; font-size: ${
         24 + 30 * level
       }pt } .annot { fill: black; font-family: sans-serif; font-size: 14pt } </style>
       <rect width='100%' height='100%' fill='${bg}' />
       ${text}
       ${text2}
    </svg>`;
};

const main2 = (attrs: Attrs): object => {
  const { emoteId, likes, level, mood, desc } = attrs;
  const image =
    "data:image/svg+xml;base64," +
    Buffer.from(svg(emoteId, level, likes)).toString("base64");

  return {
    emoteId,
    name: `NFT#${emoteId}`,
    attributes: [
      { trait_type: "Level", value: level },
      { trait_type: "Mood", value: mood },
    ],
    description: desc,
    image,
  };
};

const main = async (
  event: APIGatewayProxyEventV2
): Promise<APIGatewayProxyResultV2> => {
  // All log statements are written to CloudWatch by default. For more information, see
  // https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-logging.html
  const q = JSON.stringify(event);
  console.info(q);

  const attrs = toAttrs(parse(event.rawPath));
  return {
    statusCode: 200,
    body: JSON.stringify(main2(attrs)),
  };
};

export { main };
