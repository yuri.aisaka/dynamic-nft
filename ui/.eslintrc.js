module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: false,
  },
  plugins: ["@typescript-eslint"],
  extends: ["standard", "plugin:prettier/recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {},
};
