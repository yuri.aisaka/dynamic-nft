import { ethers } from "ethers";

// NewEmote event
export interface NewEmote {
  emoteId: number;
  name: string;
  tokenUri: string;
  likes: ethers.BigNumber;
}

type EmoteAttribute =
  | { trait_type: "Level"; value: number }
  | { trait_type: "Mood"; value: string };

// typeof json returned from tokenUri
interface EmoteMetadata {
  emoteId: number;
  name: string;
  image: string;
  description: string;
  attributes: EmoteAttribute[];
}

interface EmoteAttrs {
  likes: number;
  level: number;
  mood: string;
}

export interface Emote {
  emoteId: number;
  name: string;
  tokenUri: URL;
  imageUri: URL | string;
  description: string;
  attrs: EmoteAttrs;
}

async function fetchApi<T>(url: URL): Promise<T> {
  return fetch(url)
    .then((res) => {
      if (!res.ok) {
        throw new Error(res.statusText);
      }
      return res.json();
    })
    .catch((err: Error) => {
      console.error(err);
      throw err;
    });
}

export async function fetchEmote(event: NewEmote): Promise<Emote> {
  const tokenUri = new URL(event.tokenUri);
  console.log(`Fetching tokenURI: ${tokenUri}`);
  const emote = await fetchApi<EmoteMetadata>(tokenUri).then((metadata) => {
    return {
      emoteId: metadata.emoteId,
      name: metadata.name,
      tokenUri,
      description: metadata.description,
      attrs: {
        // Note that we intentionally omitted likes from metadata.attributes
        likes: event.likes.toNumber(),
        // HACK hard coding the order
        level: metadata.attributes[0].value as number,
        mood: metadata.attributes[1].value as string,
      },
      imageUri: metadata.image.startsWith("http")
        ? new URL(metadata.image)
        : metadata.image, // embedded resource of type data:image/svg+xml;base64
    };
  });
  return emote;
}

// fetchEmote({
//   name: "emote0",
//   likes: 10 as unknown as ethers.BigNumber,
//   tokenUri:
//     "https://w6wbwgfv5iqxm5b5opjc5wjkza0fpqco.lambda-url.ap-northeast-1.on.aws/0/level/3",
// });
