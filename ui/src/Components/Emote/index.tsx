import { ethers } from "ethers";
import { Emote } from "./emote";
import { EmoteFactory } from "../../typechain";
import "./Emote.css";

const LikeButton = ({
  emoteId,
  contract,
  onClick,
}: {
  emoteId: number;
  contract: EmoteFactory;
  onClick: () => void;
}) => {
  return (
    <button onClick={onClick} className="cta-button like-button">
      Like!
    </button>
  );
};

const MyEmotes = ({
  emotes,
  myEmoteIds,
}: {
  emotes: Emote[];
  myEmoteIds: number[];
}) => {
  return (
    <div>
      <h3>My NFTs</h3>
      <div className="my-emotes-container">
        {myEmoteIds.map((emoteId, i) => {
          return (
            <div className="my-emotes-content" key={i}>
              <ShowEmote contract={undefined} emote={emotes[emoteId]} />
            </div>
          );
        })}
      </div>
    </div>
  );
};

const AllEmotes = ({
  contract,
  emotes,
}: {
  contract: EmoteFactory;
  emotes: Emote[];
}) => {
  return (
    <div>
      <h3>All NFTs</h3>
      <div className="all-emotes-container">
        {emotes.map((emote, i) => {
          return (
            <div className="all-emotes-content" key={i}>
              <ShowEmote contract={contract} emote={emote} />
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ShowEmote = ({
  contract,
  emote,
}: {
  contract: EmoteFactory | undefined;
  emote: Emote;
}) => {
  return (
    <div className="emotes-container">
      <div className="emote-content">
        <h2>
          {emote.name} ({emote.description})
        </h2>
        <div className="image-content">
          <img src={emote.imageUri.toString()} />
        </div>
        <div className="health-bar">
          <progress value={emote.attrs.likes} max={10} />
          <p>Levelup: {`${emote.attrs.likes} / 10`}</p>
        </div>
        {contract !== undefined && (
          <div className="liking-container">
            <LikeButton
              emoteId={emote.emoteId}
              contract={contract}
              onClick={async () => {
                await contract.like(
                  emote.emoteId as unknown as ethers.BigNumber
                );
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export const ShowEmotes = ({
  contract,
  emotes,
  myEmoteIds,
}: {
  contract: EmoteFactory;
  emotes: Emote[];
  myEmoteIds: number[];
}) => {
  return (
    <div className="arena-container">
      <MyEmotes emotes={emotes} myEmoteIds={myEmoteIds} />
      <AllEmotes emotes={emotes} contract={contract} />
    </div>
  );
};
