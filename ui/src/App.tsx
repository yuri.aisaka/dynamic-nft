/* eslint no-useless-constructor: 0 */
import { ethers } from "ethers";
import React, { useEffect, useState } from "react";
import "./App.css";
import emoteFactory from "./assets/EmoteFactory.json";
import { EmoteFactory } from "./typechain";
import { ShowEmotes } from "./Components/Emote";
import { Emote, fetchEmote } from "./Components/Emote/emote";

const CONTRACT_ADDRESS = "0x225338e2f1309902AF97bf925422E1334d05E51E"; // rinkeby

class Account {
  constructor(private readonly account: string) {}

  toString(): string {
    return this.account;
  }
}

function hasMetaMask(): boolean {
  const { ethereum } = window;
  return !!ethereum;
}

function getProvider(): ethers.providers.Web3Provider {
  console.log("Instantiating Web3 provider");
  return new ethers.providers.Web3Provider(window.ethereum);
}

async function checkAccount(
  provider: ethers.providers.Web3Provider
): Promise<Account | undefined> {
  const accounts = await provider.send("eth_accounts", []);
  if (accounts.length !== 0) {
    return new Account(accounts[0]);
  }
  console.log("No account found");
}

async function requestAccount(
  provider: ethers.providers.Web3Provider
): Promise<Account | undefined> {
  const accounts: string[] = await provider.send("eth_requestAccounts", []);
  if (accounts.length !== 0) {
    return new Account(accounts[0]);
  }
  console.log("Could not connect to any account");
}

// Call this everytime we interact with the blockchain
async function getAccount(): Promise<Account | undefined> {
  const provider = getProvider();
  return (
    (await checkAccount(provider)) ??
    (await requestAccount(provider)) ??
    undefined
  );
}

async function connectWallet(
  setAccount: React.Dispatch<React.SetStateAction<Account | undefined>>,
  setFailure: React.Dispatch<React.SetStateAction<string>>
) {
  if (!hasMetaMask()) return setFailure("MetaMask not found");
  const account = await getAccount();
  if (account === undefined) return setFailure("Cannot connect to an account");
  checkNetwork(setFailure);
  setAccount(account);
}

const ConnectWallet = ({
  setAccount,
  setFailure,
}: {
  setAccount: React.Dispatch<React.SetStateAction<Account | undefined>>;
  setFailure: React.Dispatch<React.SetStateAction<string>>;
}) => {
  return (
    <div className="connect-wallet-container">
      <button
        className="cta-button connect-wallet-button"
        onClick={() => connectWallet(setAccount, setFailure)}
      >
        Connect wallet!
      </button>
    </div>
  );
};

const checkNetwork = async (
  setFailure: React.Dispatch<React.SetStateAction<string>>
) => {
  const networkVersion = window.ethereum.networkVersion;
  if (networkVersion === "4") {
    console.log("Connected to Rinkeby");
  }
  if (networkVersion === "31337") {
    console.log("Connected to Localhost");
  }
  if (!(networkVersion === "4" || networkVersion === "31337")) {
    setFailure("Please connect the wallet to Rinkeby or Localhsot");
  }
};

const FailureMsg = ({
  msg,
  setFailure,
}: {
  msg: string;
  setFailure: React.Dispatch<React.SetStateAction<string>>;
}) => {
  return (
    // ad hoc protocol: do not shot the toast when msg is empty
    <div className="toast">
      <p>{msg}</p>
      <button onClick={() => setFailure("")}>Dismiss</button>
    </div>
  );
};

const AccountPane = ({ account }: { account: Account }) => {
  return (
    <div className="mint-account gradient-text">
      account: {account.toString()}
    </div>
  );
};

const mintNft = async (contract: EmoteFactory) => {
  console.log("Going to pop wallet now to pay gas...");
  const nftTxn = await contract.mintNewEmote();
  console.log("Mining...please wait.");
  await nftTxn.wait();

  console.log(
    `Mined, see transaction: https://rinkeby.etherscan.io/tx/${nftTxn.hash}` // FIXME
  );
};

const MintButton = ({ contract }: { contract: EmoteFactory | undefined }) => {
  return contract === undefined ? (
    <button className="cta-button connect-wallet-button">
      Bad contract (bug)
    </button>
  ) : (
    <button
      onClick={() => mintNft(contract)}
      className="cta-button mint-button"
    >
      Mint NFT
    </button>
  );
};

const MintMsg = ({
  link,
  setLink,
}: {
  link: string;
  setLink: React.Dispatch<React.SetStateAction<string>>;
}) => {
  return (
    // ad hoc protocol: do not shot the toast when msg is empty
    <div className="toast">
      <p>
        Minted an NFT
        <a href={link}>{link}</a>
      </p>
      <button onClick={() => setLink("")}>Dismiss</button>
    </div>
  );
};

const loadEmoteFactory = () => {
  const provider = getProvider();
  const signer = provider.getSigner();
  return new ethers.Contract(
    CONTRACT_ADDRESS,
    emoteFactory.abi,
    signer
  ) as EmoteFactory;
};

const App = () => {
  const [failure, setFailure] = useState("");
  const [account, setAccount] = useState<Account | undefined>(undefined);
  const [nftContract, setNftContract] = useState<EmoteFactory | undefined>(
    undefined
  );
  const [link, setLink] = useState("");
  const [emotes, setEmotes] = useState<Emote[]>([]);
  const [myEmoteIds, setMyEmoteIds] = useState<number[]>([]);

  useEffect(() => {
    (async () => {
      await connectWallet(setAccount, setFailure);
    })();
  }, []);

  useEffect(() => {
    if (account === undefined) return;
    const contract = loadEmoteFactory();
    setNftContract(contract);
  }, [account]);

  const loadEmotes = async (contract: EmoteFactory, account: Account) => {
    const allEmotes = await contract.getAllEmotes();
    const newEmotes = await Promise.all(
      allEmotes.map(async ({ name, likes }, emoteId) => {
        return {
          emoteId,
          name,
          likes,
          tokenUri: await contract.tokenURI(emoteId),
        };
      })
    );
    const emotes: Emote[] = await Promise.all(newEmotes.map(fetchEmote));
    setEmotes(emotes);

    const ids = (await contract.listEmoteIdsOf(account.toString())).map(
      (id: ethers.BigNumber) => id.toNumber()
    );
    setMyEmoteIds(ids);
  };

  useEffect(() => {
    if (nftContract === undefined || account === undefined) return;
    (async () => {
      const onMint = async (
        sender: string,
        emoteId: ethers.BigNumber,
        name: string,
        likes: ethers.BigNumber
      ) => {
        console.log(
          `NewEmote { sender: ${sender}, emoteId: ${emoteId.toNumber()}, name: ${name}, likes: ${likes.toNumber()} }`
        );
        loadEmotes(nftContract, account);
      };

      const onLike = async (
        emoteId: ethers.BigNumber,
        likes: ethers.BigNumber
      ) => {
        console.log(
          `NewLike { emoteId: ${emoteId.toNumber()}, likes: ${likes.toNumber()} }`
        );
        loadEmotes(nftContract, account);
      };

      const cleanup = () => {
        nftContract.off("NewEmote", onMint);
        nftContract.off("NewLike", onLike);
      };
      nftContract.on("NewEmote", onMint);
      nftContract.on("NewLike", onLike);

      loadEmotes(nftContract, account);
      return cleanup;
    })();
  }, [nftContract]);

  return (
    <div className="App">
      <div className="container">
        <div className="header-container">
          <p className="header gradient-text">🐾 DYNAMIC NFT 🐾️</p>
          <p className="sub-text"></p>
          {failure && <FailureMsg msg={failure} setFailure={setFailure} />}
          {account === undefined ? (
            <ConnectWallet setAccount={setAccount} setFailure={setFailure} />
          ) : nftContract === undefined ? (
            <div>Contract not found (bug)</div>
          ) : (
            <div>
              <AccountPane account={account} />
              <MintButton contract={nftContract} />
              {link && <MintMsg link={link} setLink={setLink} />}
              <ShowEmotes
                contract={nftContract}
                emotes={emotes}
                myEmoteIds={myEmoteIds}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default App;
